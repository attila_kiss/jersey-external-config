package com.cipur.config.example.resource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/health-check")
public class HealthCheckResource {

    private final String userName;
    private final String password;
    private final String companyName;
    private final int timeOut;

    @Inject
    HealthCheckResource(@Named("thirdparty.username")String userName, @Named("thirdparty.password")String password,
                        @Named("thirdparty.company.name")String companyName, @Named("thirdparty.timeout")int timeOut){
        this.userName = userName;
        this.password = password;
        this.companyName = companyName;
        this.timeOut = timeOut;
    }

    @GET
    public String getHealth() {
        StringBuilder response = new StringBuilder("username: " + userName).append("<br/>password: " + password)
                .append("<br/>company name: " + companyName).append("<br/>timeout: " + timeOut);
        return response.toString();
    }

}
