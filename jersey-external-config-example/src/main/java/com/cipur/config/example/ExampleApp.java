package com.cipur.config.example;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by picur on 29/08/16.
 */
public class ExampleApp extends ResourceConfig {

    ExampleApp(){
        packages(true, "com.cipur.config");
    }
}
