package com.cipur.config.example;

import com.picur.config.Config;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {
        Map<String, Object> applicationConfig = Config.discoverConfig();
        Server jettyServer = new Server((int)applicationConfig.get("http.port"));
        configure(applicationConfig, jettyServer);
        try {
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }

    static void configure(Map applicationConfig, Server jettyServer) throws Exception {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        jettyServer.setHandler(context);

        ExampleApp app = new ExampleApp();
        Config.setConfigBinders(app, applicationConfig);

        ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(app));
        context.addServlet(jerseyServlet, "/*");
        jerseyServlet.setInitOrder(0);


    }
}
