package com.cipur.config;

import com.picur.config.exception.ConfigReaderException;
import com.picur.config.reader.JsonReader;
import com.picur.config.reader.YamlReader;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigReaderTest {

    @Test
    public void testReadJson() throws IOException {
        File jsonConfig = new File(this.getClass().getClassLoader().getResource("config.json").getFile());
        JsonReader reader = new JsonReader();
        Map config = reader.readConfig(jsonConfig);
        assertThat(config.get("thirdparty.company.name"), is("my company"));
    }

    @Test
    public void testReadYaml() throws IOException {
        File yamlConfig = new File(this.getClass().getClassLoader().getResource("application.yml").getFile());
        YamlReader reader = new YamlReader();
        Map config = reader.readConfig(yamlConfig);
        assertThat(config.get("thirdparty.company.name"), is("my company"));
    }

    @Test(expected = ConfigReaderException.class)
    public void testReadJsonWithNonExistantResource() throws IOException {
        File jsonConfig = new File(this.getClass().getClassLoader().getResource("config2.json").getFile());
        JsonReader reader = new JsonReader();
        Map config = reader.readConfig(jsonConfig);
        assertThat(config.get("thirdparty.company.name"), is("my company"));
    }
}
