package com.cipur.config;

import com.picur.config.Config;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class ConfigTest {

    @Test
    public void testConfig() throws IOException {
        Map<String, Object> conf = Config.discoverConfig();
        ResourceConfig app = new ResourceConfig();
        Config.setConfigBinders(app, conf);
        for (Map.Entry<String, Object> entry : conf.entrySet()){
            System.out.println(entry.getKey() + ": " + entry.getValue());
            System.out.println(entry.getValue().getClass());
        }
    }
}
