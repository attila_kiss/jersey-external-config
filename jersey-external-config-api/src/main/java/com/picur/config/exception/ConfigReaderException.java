package com.picur.config.exception;

public class ConfigReaderException extends RuntimeException {

    public ConfigReaderException(String message) {
        super(message);
    }

    public ConfigReaderException(String message, Throwable t) {
        super(message, t);
    }
}
