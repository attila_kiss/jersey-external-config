package com.picur.config.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.picur.config.exception.ConfigReaderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

public class JsonReader extends BaseReader {

    final Logger log = LoggerFactory.getLogger(JsonReader.class);

    @Override
    public Map<String, Object> readConfig(InputStream in) {
        Map<String, Object> config = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map raw = mapper.readValue(in, HashMap.class);
            flattenConfig("", raw, config);
            return config;
        } catch (IOException e) {
            log.error("failed to read the config.", e);
            throw new ConfigReaderException("failed to read the json config file.", e);
        }
    }

}
