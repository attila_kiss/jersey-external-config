package com.picur.config.reader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface ConfigReader {

    Map<String, Object> readConfig(InputStream in);
    Map<String, Object> readConfig(File file) throws IOException;
    Map<String, Object> readConfig(String filePath) throws IOException;

}
