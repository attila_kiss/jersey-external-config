package com.picur.config.reader;

import com.picur.config.Config;
import com.picur.config.exception.ConfigReaderException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

public abstract class BaseReader implements ConfigReader {

    @Override
    public abstract Map<String, Object> readConfig(InputStream in);

    @Override
    public Map<String, Object> readConfig(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        return readConfig(in);
    }

    @Override
    public Map<String, Object> readConfig(String filePath) throws IOException {
        InputStream in = new FileInputStream(filePath);
        return readConfig(in);
    }

    @SuppressWarnings("unchecked")
    void flattenConfig(String configName, Object raw, Map config){
        if (raw instanceof Map) {
            for (Map.Entry entry : (Iterable<Map.Entry>) ((Map) raw).entrySet()) {
                String key = (String) entry.getKey();
                Object value = entry.getValue();
                String newConfigName = configName.equals("") ? key : configName + "." + key;
                flattenConfig(newConfigName, value, config);
            }
        }else if(raw instanceof Object[]){
            for (int i = 0; i < ((Object[])raw).length; i++) {
                flattenConfig(configName + "[" + i + "]", ((Object[])raw)[i], config);
            }
        }else {
            config.put(configName, resolveConfig(raw));
        }
    }

    private Object resolveConfig(Object value){
        if (value instanceof String){
            String path = (String) value;
            if (path.startsWith("classpath:")){
                Optional<File> resolved = Optional.ofNullable(Config.findFileOnClasspath(path.substring(10, path.length())));
                return resolved.orElseThrow(() -> new ConfigReaderException("path " + path + " does not exists")).getAbsolutePath();
            }
            return path;
        }else{
            return value;
        }
    }

}
