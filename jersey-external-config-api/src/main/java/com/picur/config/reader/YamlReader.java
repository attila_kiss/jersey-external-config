package com.picur.config.reader;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class YamlReader extends BaseReader {

    @Override
    public Map<String, Object> readConfig(InputStream in) {
        Map<String, Object> config = new HashMap<>();
        Yaml yaml = new Yaml();
        Map raw = yaml.loadAs(in, HashMap.class);
        flattenConfig("", raw, config);
        return config;
    }

}
