package com.picur.config.reader;

import com.picur.config.exception.ConfigReaderException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

public class ConfigReaderProvider {

    public static ConfigReader getReader(String path) {
        if (StringUtils.isBlank(path)) {
            throw new ConfigReaderException("The configuration path was null or empty");
        }
        String extension = FilenameUtils.getExtension(path);
        ConfigReader reader;
        switch (extension) {
            case "yml":
                reader = new YamlReader();
                break;
            case "yaml":
                reader = new YamlReader();
                break;
            case "json":
                reader = new JsonReader();
                break;
            default:
                throw new ConfigReaderException("Not implemented yet: " + extension + " reader");
        }

        return reader;
    }

}
