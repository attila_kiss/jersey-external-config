package com.picur.config;

import com.picur.config.exception.ConfigReaderException;
import com.picur.config.reader.ConfigReaderProvider;
import com.picur.config.util.ChainedOptional;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Config {

    private static final List<String> FILE_NAMES = Arrays.asList("config.json", "application.json", "application.yml", "config.yml");
    private static final String CONFIG_ENV = "conf";

    public static Map<String, Object> discoverConfig() throws IOException {
        String path = findConfigPath();
        return ConfigReaderProvider.getReader(path).readConfig(path);
    }

    public static void setConfigBinders(final ResourceConfig app, final Map<String, Object> applicationConfig){

        app.register(new AbstractBinder() {

            @Override
            protected void configure() {
                for(Map.Entry<String, Object> entry : applicationConfig.entrySet()){
                    bind(entry.getValue()).named(entry.getKey());
                }
            }
        });
    }

    /**
     * Trying to find config file with the following order:
     * 1. System environment's config
     * 2. "config.json/yml" next to the compiled jar
     * 3. "config.json/yml" in the classpath
     *
     * @return the absolute path of the file
     */
    private static String findConfigPath() {

        return ChainedOptional
                .ofNullable(System.getProperty(CONFIG_ENV))
                .or(() -> FILE_NAMES.stream().filter(name -> new File("./" + name).exists())
                        .map(name -> new File("./" + name).getAbsolutePath())
                        .findFirst())
                .or(() -> FILE_NAMES.stream().filter(name -> findFileOnClasspath(name) != null)
                        .map(name -> findFileOnClasspath(name).getAbsolutePath())
                        .findFirst())
                .orElseThrow(() -> new ConfigReaderException("Cannot find valid configuration.\n" +
                        "Try setting up system environment with -D{CONFIG_ENV}={full path of the config file}\n" +
                        "Try copying the configuration file next to the application or add it to the classpath"));
    }

    public static File findFileOnClasspath(String fileName){
        URL config = Config.class.getClassLoader().getResource(fileName);
        if (config != null){
            File configFile = new File(config.getFile());
            return configFile.exists() ? configFile : null;
        }else{
            return null;
        }
    }
}
