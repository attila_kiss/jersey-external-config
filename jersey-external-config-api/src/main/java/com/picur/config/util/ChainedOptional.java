package com.picur.config.util;

import java.util.Optional;
import java.util.function.Supplier;


public class ChainedOptional<T> {

    private final Optional<T> value;

    private ChainedOptional(T value){
        this.value = Optional.ofNullable(value);
    }

    private ChainedOptional(Optional<T> value){
        this.value = value;
    }

    public ChainedOptional<T> or(Supplier<Optional<T>> supplier){
        if (!value.isPresent()){
            return new ChainedOptional<>(supplier.get());
        }
        return this;
    }

    public static <T1> Optional<T1> of(T1 t1) {
        return Optional.of(t1);
    }

    public static <T1> ChainedOptional<T1> ofNullable(T1 t1) {
        return new ChainedOptional<>(t1);
    }

    public boolean isPresent() {
        return value.isPresent();
    }

    public T get() {
        return value.get();
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> supplier) throws X {
        return value.orElseThrow(supplier);
    }
}
