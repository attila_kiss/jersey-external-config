# jersey-external-config
Small collection of helper classes to be able add external config to Jersey webservices easily.  
The usage is quite simple but a small example application can be found in the repository.  
It has dependency on Jersey libraries to make easier to inject configs with DI. For DI HK2 was used as the default implementation in Jersey is that.

There are three option to place the config file:  

* Inside the project (should be named as config.json, application.json or yaml/yml)
* next to the jar (if you planning to use embedded container and also should be named as config.json, application.json or yaml/yml)
* anywhere and provide a system variable with ```-Dconf={full path to config}```


It tries to flatten the keys in config file (so multi level structure is usable) and then optionally it's possible to bind the keys and values in HK2.  
if the value starts with "classpath:" then it tries to resolve that to full system path.

Let's say you have a config like this:

```
#!json

{
  "http.port" : 38080,
  "http.tls.keystore.path" : "classpath:path_to/resource.xml",
  "http.tls.keystore.password" : "changeit",
  "thirdparty":{
    "username": "username",
    "password": "password",
    "company": {
      "name" : "my company"
    },
    "timeout":2000
  }
}
```
In the main class you can simply call:

```
#!java

Map<String, Object> applicationConfig = Config.discoverConfig();
Server jettyServer = new Server((int)applicationConfig.get("http.port"));
```
where you have the map of the configs and what you can use for example to configure your server and later you can bind the map with HK2:
```
#!java
Config.setConfigBinders(app, applicationConfig);

```
Then in HK2 controlled objects you can inject the configs:

```
#!java

 @Inject
 HealthCheckResource(@Named("thirdparty.username")String userName, @Named("thirdparty.password")String password,
                     @Named("thirdparty.company.name")String companyName, @Named("thirdparty.timeout")int timeOut){
     this.userName = userName;
     this.password = password;
     this.companyName = companyName;
     this.timeOut = timeOut;
 }
```